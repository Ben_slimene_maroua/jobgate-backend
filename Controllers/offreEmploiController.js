const OffreEmploi= require("../Models/OffreEmploi");
const Skills= require("../Models/Skills");
const Entreprise= require("../Models/User");
const Place=require("../Models/Places")

module.exports={

createoffreEmploi: async (req,res) =>{
    try {
        const newOffre= new OffreEmploi(req.body);
        const offreEmploi= await newOffre.save();
//await Skills.findByIdAndUpdate(req.body.skills , {$push:{offreemploi:offreEmploi}})
await Entreprise.findByIdAndUpdate(req.body.entreprise, {
    $push: { offres: offreEmploi },    });

await Place.findByIdAndUpdate(req.body.place, {
    $push:{offres: offreEmploi}
});
        res.status(201).json({
            message:"offre ajouté avec succé",
            data: offreEmploi,
        });
    } catch (error) {
        res.status(500).json({
            message: error.message
        });
    }
},
getOffreEmploi: async (req,res)=>{
try {
    const offreEmploi= await OffreEmploi.find({}).populate("entreprise").populate("place").populate("specialite").populate("candidatures");
    res.status(200).json({
        message: "la liste des offres est:",
        data: offreEmploi,
    });
} catch (error) {
    res.status(500).json({
        message: error.message
    });
}
},

getOffreById: async (req,res)=>{
    try {
        const offreEmploi= await OffreEmploi.findById({_id:req.params.id}).populate("entreprise").populate("place").populate("specialite").populate("candidatures");
        res.status(200).json({
            message: "votre offres est:",
            data: offreEmploi,
        });
    } catch (error) {
        res.status(500).json({
            message: error.message
        });
    }
    },

 updateOffre: async (req,res)=>{
        try {
            await OffreEmploi.updateOne({_id:req.params.id}, req.body)
            res.status(201).json({
                message:"votre offre est modifié",
            })
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },
 deleteOffre: async(req,res)=>{
        try {
            await OffreEmploi.deleteOne({_id: req.params.id});
            res.status(201).json({
                message: "votre offre est supprimé",
              });
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

}