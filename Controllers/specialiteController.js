const Specialite= require("../Models/Specialite");


module.exports={
    createSpecialite: async (req,res)=>{
        try {
            const newSpecialite= new Specialite(req.body);
            const specialite= await newSpecialite.save();
            res.status(201).json({
                message:"Specialite ajouté",
                data: specialite,
            });
            
        } catch (error) {
            res.status(500).json({
                message: error.message
            });
        }
    },

    getSpecialite: async(req,res)=>{
        try {
          const specialite= await Specialite.find({}).populate("entreprises");
          res.status(200).json({
              message: "la liste des Specialites est:",
              data: specialite,
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            })
            
        }
    },

    getSpecialiteById: async (req,res)=>{
        try {
           const specialite= await Specialite.findById({_id: req.params.id}) ;
           res.status(201).json({
            message: "voila votre speialité",
            data: specialite
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

    updateSpecialite: async (req,res)=>{
        try {
            await Specialite.updateOne({_id:req.params.id}, req.body)
            res.status(201).json({
                message:"votre Specialite est modifié",
            })
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

    deleteSpecialite: async(req,res)=>{
        try {
            await Specialite.deleteOne({_id: req.params.id});
            res.status(201).json({
                message: "votre Specialite est supprimé",
              });
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },
}