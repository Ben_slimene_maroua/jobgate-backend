const Category= require("../Models/Category");


module.exports={
    createCategory: async (req,res)=>{
        try {
            const newcategory= new Category(req.body);
            const category= await newcategory.save();
            res.status(201).json({
                message:"categorie ajouté",
                data: category,
            });
            
        } catch (error) {
            res.status(500).json({
                message: error.message
            });
        }
    },

    getCategory: async(req,res)=>{
        try {
          const category= await Category.find({});
          res.status(200).json({
              message: "la liste des categories est:",
              data: category,
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            })
            
        }
    },

    getCategoryById: async (req,res)=>{
        try {
           const category= await Category.findById({_id: req.params.id}) ;
           res.status(201).json({
            message: "voila votre categorie",
            data: category
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

    updateCategory: async (req,res)=>{
        try {
            await Category.updateOne({_id:req.params.id}, req.body)
            res.status(201).json({
                message:"votre categorie est modifié",
            })
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

    deleteCategory: async(req,res)=>{
        try {
            await Category.deleteOne({_id: req.params.id});
            res.status(201).json({
                message: "votre categorie est supprimé",
              });
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },
}