const Places= require("../Models/Places");


module.exports={
    createPlaces: async (req,res)=>{
        try {
            const newPlaces= new Places(req.body);
            const places= await newPlaces.save();
            res.status(201).json({
                message:"Place ajouté",
                data: places,
            });
            
        } catch (error) {
            res.status(500).json({
                message: error.message
            });
        }
    },

    getPlaces: async(req,res)=>{
        try {
          const places= await Places.find({});
          res.status(200).json({
              message: "la liste des places est:",
              data: places,
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            })
            
        }
    },

    getPlacesById: async (req,res)=>{
        try {
           const places= await Places.findById({_id: req.params.id}) ;
           res.status(201).json({
            message: "voila votre lieu",
            data: places
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

    updatePlaces: async (req,res)=>{
        try {
            await Places.updateOne({_id:req.params.id}, req.body)
            res.status(201).json({
                message:"votre Place est modifié",
            })
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

    deletePlaces: async(req,res)=>{
        try {
            await Places.deleteOne({_id: req.params.id});
            res.status(201).json({
                message: "votre Place est supprimé",
              });
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },
}