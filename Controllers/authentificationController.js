const User = require("../Models/User");
const Specialite= require("../Models/Specialite");
const OffreEmploi= require("../Models/OffreEmploi");
const bcrypt = require("bcrypt");
const { randomBytes } = require("crypto");
const jwt = require("jsonwebtoken");
const DOMAIN = process.env.APP_DOMAIN;
const nodemailer = require("nodemailer");
const SECRET = process.env.APP_SECRET;
var RefrechTokens=[];
const transporteur = nodemailer.createTransport({
  service: "gmail",
  auth: { user: process.env.APP_USER, pass: process.env.APP_PASS },
});
module.exports = {
  registreAdmin: async (req, res) => {
    try {
      req.body["image"] = req.file.filename;
      const password = bcrypt.hashSync(req.body.password, 10);
      const newregAdmin = {
        ...req.body,
        password,
        role: "admin",
      };
      const admin = await User.create(newregAdmin);
      res.status(201).json({
        message: "Admin creé avec succée",
        data: admin,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  registre: async (req, res) => {
    try {
     req.body["image"] = req.file.filename;
    const password = bcrypt.hashSync(req.body.password, 10);
    const newUser = new User({
      ...req.body,
      password,
    });

    const user = await User.create(newUser);


   
    await Specialite.findByIdAndUpdate(req.body.specialites, {$push: {entreprises:user}});
   

    res.status(201).json({
      message: "Your account is created",
      data: user,
    });  

    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
getentreprise: async(req,res)=>{
  try {
    const user= await User.find({}).populate("specialites");


   return  res.status(200).json({
      data: user,
      });    console.log(user)
    
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  } 
},
getUserById: async(req,res)=>{
  try {
    const user= await User.findById({_id:req.params.id}).populate("specialites");


   return  res.status(200).json({
      data: user,
      });    console.log(user)
    
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  } 
},

  deleteUser: async (req,res)=>{
try {

  await User.deleteOne({ _id: req.params.id });
  res.status(200).json({
    message: "utilisateur supprimé",
  });
} catch (error) {
  res.status(500).json({
    message: error.message,
  });
}
  },
 
  confirmedUser: async(req,res)=>{
    try {
      const user=await User.findById({_id:req.params.id});
      user.verifier=true;
      await user.save();
      transporteur.sendMail({
        to: user.email,
        subject: "bienvenue" + user.nom,
        text: " Bonjour",
        html: `<!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>welcome email</title>
                </head>
                <body>
                  <h2 >Hello   ${user.nom}</h2>  
                  <p>we are glad to have you on board at  ${user.email}</p>
                  <a href="">thank you for joining our plateform</a>
                </body>
                </html>`,
      },
      function(err,info){
        if(err){console.log("error:",err);}
        else{console.log("email envoyé:", info.response);}
      }
      );
      res.status(201).json({
        message:"user verifié et confirmé",
        data: user,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  login: async (req, res) => {
    try {
    const{email,password}= req.body;
     
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(500).json({
          message: "email non validé",
        });
      }
      const passCompare = bcrypt.compareSync(password, user.password);
      if (!passCompare) {
        return res.status(500).json({
          message: "password incorect",
        });
      }
     
       else {
        const token = jwt.sign({ id: user._id, user: user }, SECRET, {
          expiresIn: "7 days",
        });
        const refrechToken = jwt.sign({ id: user._id }, SECRET, {
          expiresIn: "24h",
        });
        RefrechTokens[refrechToken] = user._id;
  
        const restok = {
          email: user.email,
          user: user,
          token: token,
          expiresIn: 1,
          refrechToken: refrechToken,
        };
        res.status(201).json({
          message: "login validé avec succée",
          ...restok,
        });
      }
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  refrechToken : async (req, res) => {
    try {
      const refrechToken = req.body.refrechToken;
    
      if (refrechToken in RefrechTokens) {
        const token = jwt.sign({ id: req.user._id }, SECRET, { expiresIn: "7h" });
        const refrechToken = jwt.sign({ id: req.user._id }, SECRET, {
          expiresIn: "24h",
        });
        RefrechTokens[refrechToken] = req.user._id;
        res.status(200).json({
          accesstoken: token,
          refrechToken: refrechToken,
        });
      } else {
        res.status(404).json({
          status: 404,
          message: "refresh token not exist",
        });
      }
    } catch (error) {
      res.status(404).json({
        status: 404,
        message: error.message,
      });
    }
  },
  profile: async (req, res) => {
    try {
      const user = req.user;
      res.status(200).json({ message: "you are authentify", data: user });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },

  forgetpassword : async (req, res) => {
    try {
      const email = req.body.email;
      const user = await User.findOne({ email});
    //  console.log(user);
      if (!user) {
        return res.status(500).json({ message: "Your email does not exist" });
      }
      const token = jwt.sign({ id: user._id }, SECRET, {
        expiresIn: "2h",
      });
      console.log("resettttt :", token);
      await User.findOneAndUpdate(
        { email: email },
        { resetPasswordToken: token }
      );
    
      transporteur.sendMail(
        {
          to: user.email,
          subject: "Forget Password",
          text: "Bonjour MR",
          html: `<!DOCTYPE html>
          <html lang="en">
          <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <title>Welcome Email</title>
          </head>
          <body>
              <h2>
                  Hello ${user.nom}
              </h2>
              <p>We are glad to have you on bord at ${user.email}</p>
              <a href="${DOMAIN}/reset/${token}">Reset Password</a>
          </body>
          </html>`,
        },
         //fonction pour afficher msg lors de la creation de client lors de lenvoi du email
    function (err, info) {
      if (err) {
        console.log("error : " + err.message);
      } else {
        console.log("email send : " + info.res);
      }
    }
  );
 return res.status(200).json({
    message: "your compte is created verify your email",
  });
res.status(200).json({ message: "email send" });
} catch (error) {
  res.status(500).json({
    message: error.message,
  });
}
},

resetpassword :async (req, res) => {
  try {
    const resetPasswordToken = req.body.resetPasswordToken;
    if (resetPasswordToken) {
      //pour verifier date d'expiration de resetPasswordToken
      jwt.verify(resetPasswordToken, SECRET, async (err) => {
        if (err) {
          return res.json({
            error: "incorrect token or it is expired",
          });
        }
        const user = await User.findOne({
          resetPasswordToken: resetPasswordToken,
        });
        user.password = bcrypt.hashSync(req.body.newpassword, 10);
        user.save();
        return res.status(200).json({
          message: "password has been changed",
        });
      });
    }
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
},
changePassword : async (req, res) => {
  try {
    const user = await req.user;
    const comp = bcrypt.compareSync(req.body.oldpassword, user.password);
    if (!comp) {
      res.status(500).json({ message: "old password is false" });
    } else {
      user.password = bcrypt.hashSync(req.body.newpassword, 10);
      user.save();
      res.status(200).json({ message: "password changed" });
    }
  } catch (error) {
    res.status(500).json({ message: error.meessage });
  }
},
confirmedOffre: async(req,res)=>{
  try {
    const offre=await OffreEmploi.findById({_id:req.params.id});
    const user = await User.findOne({ offres: offre });
    if (!user) {
      res.status(500).json({ message: "User indifiened" });
    }
    offre.verifier=true;
    await offre.save();
    transporteur.sendMail({
      to: user.email,
      subject: "bienvenue" + user.nom,
      text: " Bonjour",
      html: `<!DOCTYPE html>
              <html lang="en">
              <head>
                  <meta charset="UTF-8">
                  <meta http-equiv="X-UA-Compatible" content="IE=edge">
                  <meta name="viewport" content="width=device-width, initial-scale=1.0">
                  <title>welcome email</title>
              </head>
              <body>
                <h2 >Hello   ${user.nom}</h2>  
                <p>we are glad to have you on board at  ${user.email}</p>
                <a href="">thank you for joining our plateform</a>
              </body>
              </html>`,
    },
    function(err,info){
      if(err){console.log("error:",err);}
      else{console.log("email envoyé:", info.response);}
    }
    );
    res.status(201).json({
      message:"offre verifié et confirmé",
      data: user,
    });
  } catch (error) {

  }
},
};
