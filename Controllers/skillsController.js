const Skills= require("../Models/Skills");


module.exports={
    createSkills: async (req,res)=>{
        try {
            const newSkills= new Skills(req.body);
            const skills= await newSkills.save();
            res.status(201).json({
                message:"Skills ajouté",
                data: skills,
            });
            
        } catch (error) {
            res.status(500).json({
                message: error.message
            });
        }
    },

    getSkills: async(req,res)=>{
        try {
          const skills= await Skills.find({});
          res.status(200).json({
              message: "la liste des Skills est:",
              data: skills,
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            })
            
        }
    },

    getSkillsById: async (req,res)=>{
        try {
           const skills= await Skills.findById({_id: req.params.id}) ;
           res.status(201).json({
            message: "voila votre Skills",
            data: skills
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

    updateSkills: async (req,res)=>{
        try {
            await Skills.updateOne({_id:req.params.id}, req.body)
            res.status(201).json({
                message:"votre Skills est modifié",
            })
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

    deleteSkills: async(req,res)=>{
        try {
            await Skills.deleteOne({_id: req.params.id});
            res.status(201).json({
                message: "votre Skills est supprimé",
              });
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },
}