const Message= require("../Models/Message");
const Entreprise=require("../Models/User");
module.exports={
    createMessage: async (req,res)=>{
        try {
            const newMsg= new Message(req.body);
            const msg= await newMsg.save();
            await Entreprise.findByIdAndUpdate(req.body.entreprise, {
                $push: { messages: msg },
              });
              await Entreprise.findByIdAndUpdate(req.body.candidat, {
                $push: { messages: msg },
              }); 
            res.status(201).json({
                message:"message envoyé",
                data: msg,
            });
            
        } catch (error) {
            res.status(500).json({
                message: error.message
            });
        }
    },

    getMessage: async(req,res)=>{
        try {
          const msg= await Message.find({}).populate("entreprise").populate("candidat");
          res.status(200).json({
              message: "la liste des messages est:",
              data: msg,
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            })
            
        }
    },

    getMessageById: async (req,res)=>{
        try {
           const msg= await Message.findById({_id: req.params.id}).populate("candidat") ;
           res.status(201).json({
            message: "voila votre message",
            data: msg
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

   

    deleteMessage: async(req,res)=>{
        try {
            await Message.deleteOne({_id: req.params.id});
            res.status(201).json({
                message: "message  supprimé",
              });
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },
}