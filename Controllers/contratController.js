const Contrat= require("../Models/Contrat");


module.exports={
    createContrat: async (req,res)=>{
        try {
            const newContrat= new Contrat(req.body);
            const contrat= await newContrat.save();
            res.status(201).json({
                message:"Contrat ajouté",
                data: contrat,
            });
            
        } catch (error) {
            res.status(500).json({
                message: error.message
            });
        }
    },

    getContrat: async(req,res)=>{
        try {
          const contrat= await Contrat.find({});
          res.status(200).json({
              message: "la liste des Contrat est:",
              data: contrat,
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            })
            
        }
    },

    getContratById: async (req,res)=>{
        try {
           const contrat= await Contrat.findById({_id: req.params.id}) ;
           res.status(201).json({
            message: "voila votre Contrat",
            data: contrat
          });
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

    updateContrat: async (req,res)=>{
        try {
            await Contrat.updateOne({_id:req.params.id}, req.body)
            res.status(201).json({
                message:"votre Contrat est modifié",
            })
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },

    deleteContrat: async(req,res)=>{
        try {
            await Contrat.deleteOne({_id: req.params.id});
            res.status(201).json({
                message: "votre Contrat est supprimé",
              });
            
        } catch (error) {
            res.status(500).json({
                message:error.message
            });
        }
    },
}