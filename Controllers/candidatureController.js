const Candidature= require("../Models/Candidature");
const Offre = require("../Models/OffreEmploi");
const Candidat = require("../Models/User");
createcandidature = async   (req, res)  =>  {
     try {
      req.body["cv"] = req.file.filename;
     const newCandidature = new Candidature(req.body);
     const candidature = await  newCandidature.save();
    await Offre.findByIdAndUpdate(req.body.id_offre, {
      $push: { candidatures: candidature },
    });
    await  Candidat.findByIdAndUpdate(req.body.candidat, {
      $push: { candidatures: candidature },
    });
   res.status(201).json(
       {
           message : 'candidature created',
           data :candidature
       });
     } catch (error) {
       res.status(500).json({
         message: error,
         statut: 500,
       });
     }
   };
getallsCandidature = async(req,res) =>
   {
       //
       try {
         const listscandidature= await Candidature.find({}).populate("candidat").populate("id_offre");
        // appel du modéle Category
         res.status(200).json({
           message: "this is the list of candidature",
           data: listscandidature,
         });
       } catch (error) {
         res.status(500).json({
           message: error.message,
         });
       }
     },
getcandidaturetById = async (req, res) => {
       try {
         const candidature= await Candidature.findById({ _id: req.params.id }).populate("candidat").populate("id_offre"); // recherche by id et la déclaration d'id dans le base de donnée _id
         res.status(200).json({
           message: " this is a candidature",
           data: candidature,
         });
       } catch (error) {
         res.status(500).json({
           message: error.message,
         });
       }
     },
getcandidatureByname = async (req, res) => {
       try {
         const candidature = await Candidature.find({date: req.query.date}).populate("candidat").populate("id_offre"); // recherche by id et la déclaration d'id dans le base de donnée _id
         res.status(200).json({
           message: " this is a candidature by name",
           data: candidature,
         });
       } catch (error) {
         res.status(500).json({
           message: error.message,
         });
       }
     },
    Updatecandidature= async (req, res) => {
       try {
          await Candidature.updateOne({_id: req.params.id}, req.body); // recherche by id et la déclaration d'id dans le base de donnée _id
         res.status(200).json({
           message: " your candidature is updated",
         });
       } catch (error) {
         res.status(500).json({
           message: error.message,
         });
       }
     };
Deletecandidature= async (req, res) => {
       try {
         await Candidature.deleteOne({_id: req.params.id}); // recherche by id et la déclaration d'id dans le base de donnée _id
         res.status(200).json({
           message: " your candidature is deleted",
         });
       } catch (error) {
         res.status(500).json({
           message: error.message,
         });
       }
     },
     module.exports = {createcandidature ,getallsCandidature ,getcandidaturetById  ,getcandidatureByname, Updatecandidature,
        Deletecandidature }