const express=require("express");
const cors=require("cors");
const { success, error } = require("consola");
require("dotenv").config();
const dbjobgate = require("./Config/db");
const port= process.env.APP_Port || 5000 ;
const domain=process.env.APP_DOMAIN; 
const authRoute=require("./Routes/authRoute");
const adminRoute= require("./Routes/adminRoute");
const categoryRoute= require("./Routes/categoryRoute");
const specialiteRoute= require("./Routes/specialiteRoute");
const skillsRoute= require("./Routes/skillsRoute");
const placesRoute=require("./Routes/placesRoute");
const contratRoute=require("./Routes/contratRoute");
const offreRoute= require("./Routes/offreEmploiRoute");
const testRoute= require("./Routes/testRoute");
const candidatureRoute= require("./Routes/candidatureRoute");
const messageRoute= require("./Routes/messageRoute")


const app= express();

app.use(express.json());
app.use(cors());

app.use("/registre",authRoute);
app.use("/admin",adminRoute);
app.use("/category", categoryRoute);
app.use("/specialite", specialiteRoute);
app.use("/skills", skillsRoute);
app.use("/places",placesRoute);
app.use("/contrat",contratRoute);
app.use("/offre", offreRoute);
app.use("/test",testRoute);
app.use("/candidature",candidatureRoute);
app.use("/message",messageRoute);

app.get("/getfile/:image",function(req,res){
  res.sendFile(__dirname+'/storages/'+req.params.image)
})


app.listen(port, async () => {
    try {
      success({
        message: `Server started on PORT ${port} ` + `URL : ${domain}`,
        badge: true,
      });
    } catch (err) {
      error({ message: "error with server", badge: true });
    }
  });