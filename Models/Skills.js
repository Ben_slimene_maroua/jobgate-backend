const mongoose= require("mongoose")
const schemaskills= new mongoose.Schema({
    outils:{
        type:String,
        required: false,
    },
    description:{
        type: String,
        required:false,
    },
   offreemploi:{
    type:mongoose.Types.ObjectId,
    ref:'OffreEmploi',
   }
})
module.exports= mongoose.model("Skills", schemaskills);