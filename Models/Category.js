const mongoose= require("mongoose")
const schemacategory= new mongoose.Schema({
    nom:{
        type:String,
        required: false,
    },
    description:{
        type: String,
        required:false,
    },
})
module.exports= mongoose.model("Category", schemacategory);