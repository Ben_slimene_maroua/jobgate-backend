const mongoose= require("mongoose")
const schemaplaces= new mongoose.Schema({
    adresse:{
        type:String,
        required: false,
    },
    offres:[{
        type: mongoose.Types.ObjectId,
        ref: "OffreEmploi",
    }
]
})
module.exports= mongoose.model("Places", schemaplaces);