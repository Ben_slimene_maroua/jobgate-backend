const mongoose = require("mongoose");
const CandidatureSchema = new mongoose.Schema(
  {
    date: {
      type: String,
    },
    cv: {
      type: String,
      trim: true,
      required: true,
    },
    candidat : {
      type : mongoose.Types.ObjectId,
      ref : 'User',
      required: false,
    },
    id_offre : {
      type : mongoose.Types.ObjectId,
      ref : 'OffreEmploi',
      required: false,
    }
  },
  { timestamps: true }
);
module.exports = mongoose.model("Candidature", CandidatureSchema);