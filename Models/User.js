const mongoose = require("mongoose");

const schemauser = new mongoose.Schema({
  nom: {
    type: String,
    required: false,
  },
  prenom: {
    type: String,
    required: false,
  },
  adresse: {
    type: String,
    required: false,
  },
  telephone: {
    type:String,
    required: false,
  },
 
  role: {
    type: String,
    enum: ["candidat", "admin", "entreprise"],
    required:false
  },
  email: {
    type: String,
    required: false,
   
  },
  password: {
    type: String,
    required: false,
  },
  image: {
    type: String,
    required: false,
  },
  resetPasswordToken: {
    type: String,
    required: false,
  },
  resetPassworedExpiresIn: {
    type: Date,
    required: false,
  },
  refrechToken:{
    type:String,
    required:false,
  },
  verifier: {
    type: Boolean,
    default: false,
  },
  siteweb:{
    type:String,
    required:false
  },
  description:{
    type:String,
    required:false
  },
 
  specialites:
    {
      type:mongoose.Types.ObjectId,
      ref:'Specialite'
    }
  ,
  offres:[
    {
      type:mongoose.Types.ObjectId,
      ref:'OffreEmploi'
    }
  ],
  place:{
    type: mongoose.Types.ObjectId,
    ref:'Places'
  },
  candidatures:[
    {
      type:mongoose.Types.ObjectId,
      ref:'Candidature'
    }
  ],
  messages:[
    {
      type:mongoose.Types.ObjectId,
      ref:'Message'
    }
  ],

});
module.exports = mongoose.model("User", schemauser);
