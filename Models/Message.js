const mongoose= require("mongoose")
const schemamessage= new mongoose.Schema({

    titre:{
        type: String,
        required: true
    },
    description:{
        type: String,
        required:true
    },
    entreprise:{
        type: mongoose.Types.ObjectId,
        ref:'User'
    },
    candidat:{
        type: mongoose.Types.ObjectId,
        ref:'User'
    }

})
module.exports= mongoose.model("Message", schemamessage);