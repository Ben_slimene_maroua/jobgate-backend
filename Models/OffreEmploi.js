const mongoose= require("mongoose")
const schemaoffreEmploi= new mongoose.Schema({
    titre:{
        type:String,
        required: false,
    },
    type:{
        type: String,
        required:false,
    },
   date:{
        type:String,
        required:false,
    },
    description:{
        type: String,
        required:false,
    },
   
      entreprise:{
          type:mongoose.Types.ObjectId,
          ref:'User'
      },
      verifier: {
        type: Boolean,
        default: false,
      },
      place:{
          type: mongoose.Types.ObjectId,
          ref:"Places"
      },
      specialite:{
        type: mongoose.Types.ObjectId,
        ref:"Specialite"
      },
      candidatures:[
        {
          type:mongoose.Types.ObjectId,
          ref:'Candidature'
        }
      ],
})
module.exports= mongoose.model("OffreEmploi", schemaoffreEmploi);