const mongoose= require("mongoose")
const schemaspecialite= new mongoose.Schema({
    nom:{
        type:String,
        required: false,
    },
    description:{
        type: String,
        required:false,
    },
    entreprises:[{
        type:mongoose.Types.ObjectId,
        ref:'User',
    }],
    category:{
        type:mongoose.Types.ObjectId,
        ref:'Category',
    }

})
module.exports= mongoose.model("Specialite", schemaspecialite);