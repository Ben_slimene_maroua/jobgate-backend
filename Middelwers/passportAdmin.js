const {Strategy,ExtractJwt}= require("passport-jwt");
const passport=require("passport");
const SECRET= process.env.APP_SECRET;
const User=require("../Models/User");

var options={
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: SECRET,
};
passport.use(
    new Strategy(options,async ({id}, done)=>{
        try {
           const user= await User.findById(id);
           if(!user){
               throw new Error("user not defined");
           }
           if(user.role ==="admin"){
          return  done(null, user);
           } 
           else{
               throw new Error("you are not admin");
           }
        } catch (error) {
          return done(null,error.message); 
        }
    })
);