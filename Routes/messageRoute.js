const express= require("express");
const route= require("express").Router();
const messageController=require("../Controllers/messageController");
const passport= require("passport")



route.post("/createmessage" ,messageController.createMessage);
route.get("/getmessage",messageController.getMessage);
route.get("/getmessageById/:id" ,messageController.getMessageById);
route.delete("/deletemessage/:id" ,messageController.deleteMessage);
module.exports=route;