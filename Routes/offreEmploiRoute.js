const express= require("express");
const route= require("express").Router();
const offreEmploiController=require("../Controllers/offreEmploiController");
const passport= require("passport")
require("../Middelwers/passport_authentification").passport;

route.post("/createoffre",offreEmploiController.createoffreEmploi);
route.get("/getoffre",offreEmploiController.getOffreEmploi);
route.get("/getoffreById/:id",offreEmploiController.getOffreById);
route.put("/upoffre/:id",passport.authenticate("jwt", {session: false}),offreEmploiController.updateOffre);
route.delete("/deleteoffre/:id",passport.authenticate("jwt", {session: false}),offreEmploiController.deleteOffre);

module.exports=route;