const express= require("express");
const route= require("express").Router();
const skillsController=require("../Controllers/skillsController");
const passport= require("passport")
require("../Middelwers/passportAdmin").passport;


route.post("/createskills", passport.authenticate("jwt",{session:false}) ,skillsController.createSkills);
route.get("/getskills", passport.authenticate("jwt",{session:false}) ,skillsController.getSkills);
route.get("/getskillsById/:id", passport.authenticate("jwt",{session:false}) ,skillsController.getSkillsById);
route.put("/upskills/:id", passport.authenticate("jwt",{session:false}) ,skillsController.updateSkills);
route.delete("/deleteskills/:id", passport.authenticate("jwt",{session:false}) ,skillsController.deleteSkills);
module.exports=route;