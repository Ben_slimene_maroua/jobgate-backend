const route = require("express").Router();
const authentification = require("../Controllers/authentificationController");
const passport = require("passport");
require("../Middelwers/passportAdmin").passport;

route.get(
  "/profile",
  passport.authenticate("jwt", { session: false }),
  authentification.profile
);

route.put(
  "/confirmuser/:id",
  passport.authenticate("jwt", { session: false }),
  authentification.confirmedUser
);
route.put(
  "/confirmoffre/:id",
  passport.authenticate("jwt", { session: false }),
  authentification.confirmedOffre
);
route.put(
  "/changepassword",
  passport.authenticate("jwt", { session: false }),
  authentification.changePassword
);
route.get(
  "/refreshtoken",
  passport.authenticate("jwt", { session: false }),
  authentification.refrechToken
);

route.delete(
  "/deleteuser/:id",
  passport.authenticate("jwt", { session: false }),
  authentification.deleteUser
);
route.post("/forgetpassword",authentification.forgetpassword);
route.post("/resetpassword",authentification.resetpassword);
route.get("/getentreprise",authentification.getentreprise);
route.get("/getUserById/:id",authentification.getUserById);
module.exports = route;
