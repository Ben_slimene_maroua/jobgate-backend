const express= require("express");
const route= require("express").Router();
const contratController=require("../Controllers/contratController");
const passport= require("passport")
require("../Middelwers/passportAdmin").passport;


route.post("/createcontrat", passport.authenticate("jwt",{session:false}) ,contratController.createContrat);
route.get("/getcontrat", passport.authenticate("jwt",{session:false}) ,contratController.getContrat);
route.get("/getcontratById/:id", passport.authenticate("jwt",{session:false}) ,contratController.getContratById);
route.put("/upcontrat/:id", passport.authenticate("jwt",{session:false}) ,contratController.updateContrat);
route.delete("/deletecontrat/:id", passport.authenticate("jwt",{session:false}) ,contratController.deleteContrat);
module.exports=route;