const route= require("express").Router();
const authentification= require("../Controllers/authentificationController");
const uploadImage= require("../Middelwers/uploadImage");
const passport= require("passport");
require("../Middelwers/passport_authentification").passport;



route.post("/admin",uploadImage.single("image"),authentification.registreAdmin);
route.post("/user",uploadImage.single("image"),authentification.registre);
//route.post("/user",authentification.registre);
route.post("/login",authentification.login);
route.get("/getentreprise",authentification.getentreprise);
route.get("/getUserById/:id",authentification.getUserById);
route.put("/changepassword",passport.authenticate("jwt", { session: false }), authentification.changePassword );
route.post("/forgetpassword",authentification.forgetpassword);
route.post("/resetpassword",authentification.resetpassword);
module.exports=route;