const express= require("express");
const route= require("express").Router();
const specialiteController=require("../Controllers/specialiteController");
const passport= require("passport")
require("../Middelwers/passportAdmin").passport;


route.post("/createspecialite", passport.authenticate("jwt",{session:false}) ,specialiteController.createSpecialite);
route.get("/getspecialite",specialiteController.getSpecialite);
route.get("/getspecialiteById/:id", passport.authenticate("jwt",{session:false}) ,specialiteController.getSpecialiteById);
route.put("/upspecialite/:id", passport.authenticate("jwt",{session:false}) ,specialiteController.updateSpecialite);
route.delete("/deletespecialite/:id", passport.authenticate("jwt",{session:false}) ,specialiteController.deleteSpecialite);
module.exports=route;