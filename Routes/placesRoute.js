const express= require("express");
const route= require("express").Router();
const placesController=require("../Controllers/placesController");
const passport= require("passport")
require("../Middelwers/passportAdmin").passport;


route.post("/createplaces", passport.authenticate("jwt",{session:false}) ,placesController.createPlaces);
route.get("/getplaces" ,placesController.getPlaces);
route.get("/getplacesById/:id",placesController.getPlacesById);
route.put("/upplaces/:id", passport.authenticate("jwt",{session:false}) ,placesController.updatePlaces);
route.delete("/deleteplaces/:id", passport.authenticate("jwt",{session:false}) ,placesController.deletePlaces);
module.exports=route;