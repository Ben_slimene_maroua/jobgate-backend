const route = require("express").Router();
const candidaturecontroller = require("../Controllers/candidatureController");
const uploadimage = require("../Middelwers/uploadImage")
const passport_admin = require("passport");
require("../Middelwers/passportAdmin").passport;
route.post(
  "/createcandidature",  uploadimage.single("cv"),
  candidaturecontroller.createcandidature
);
route.get("/getallcandidature", candidaturecontroller.getallsCandidature);
route.get("/getcandidaturebyid/:id", candidaturecontroller.getcandidaturetById);
route.get("/getcandidaturebyname", candidaturecontroller.getcandidatureByname);
route.put(
  "/updatecandidature/:id",
  candidaturecontroller.Updatecandidature
);
route.delete(
  "/deletecandidature/:id",
  candidaturecontroller.Deletecandidature
);
module.exports = route;