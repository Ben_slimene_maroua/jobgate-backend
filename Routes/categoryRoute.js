const express= require("express");
const route= require("express").Router();
const categoryController=require("../Controllers/categoryController");
const passport= require("passport")
require("../Middelwers/passportAdmin").passport;


route.post("/createcateg", passport.authenticate("jwt",{session:false}) ,categoryController.createCategory);
route.get("/getcateg", passport.authenticate("jwt",{session:false}) ,categoryController.getCategory);
route.get("/getcategById/:id", passport.authenticate("jwt",{session:false}) ,categoryController.getCategoryById);
route.put("/upcateg/:id", passport.authenticate("jwt",{session:false}) ,categoryController.updateCategory);
route.delete("/deletecateg/:id", passport.authenticate("jwt",{session:false}) ,categoryController.deleteCategory);
module.exports=route;